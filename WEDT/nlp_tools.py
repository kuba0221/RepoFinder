import json
import nltk
import os
import re
import pickle
from sklearn.metrics.pairwise import linear_kernel
from sklearn.feature_extraction.text import TfidfVectorizer
from scipy import sparse

underscore_at_word_end_regex = re.compile(r'(\b_+|_+\b)')

tokenizer = nltk.tokenize.RegexpTokenizer('\w+')

stemmer = nltk.stem.porter.PorterStemmer()

class Repository:
    def __init__(self, name, readme_text, description):
        self.name = name
        self.readme_text = readme_text
        if description:
            self.description = description
        else:
            self.description = "None"
        

    def __str__(self):
        string = "Name: " + self.name + '\n'
    
        string += "Description: " + self.description + '\n'

        string += "Readme: " +  self.readme_text + '\n'

        return string

class Readme_tf_idf:
    def __init__(self, model_file):
        with open(model_file, 'rb') as f:
            self.model = pickle.load(f)

    def vectorize(self, text):
        vector = self.model.transform(text)
        return vector

def get_repositories_json_files(repositories_directory):
    json_files = os.listdir(repositories_directory)
    json_list = []
    for json_file in json_files:
        with open(os.path.join(repositories_directory, json_file)) as f:
            json_list.append(json.load(f))
    return json_list  
    
def get_training_documents(repositories_directory):

    training_documents = get_reuters_articles('training')
    if repositories_directory:
         json_list = get_repositories_json_files(repositories_directory)
         training_documents += [json['readme'] for json in json_list if is_ascii(json['readme'])]
    return training_documents  
   
def get_reuters_articles(article_type):
    reuters_articles = []
    for reuters_article in filter(lambda f: f.startswith(article_type), nltk.corpus.reuters.fileids()):
        article_text = " ".join(lowercase_first_uppercase_words(list(nltk.corpus.reuters.words(reuters_article))))

        reuters_articles.append(article_text)

    return reuters_articles

def preprocess(text):
    return re.sub(underscore_at_word_end_regex, " ", text)

def is_ascii(s):
    return all(ord(c) < 128 for c in s)

def tokenize(text):
    tokens = tokenizer.tokenize(text)
    stems = []
    for token in tokens:
        if token.isalnum():
            stems.append(stemmer.stem(token))
    return stems

def lowercase_first_uppercase_words(article_words):
    article_words[0] = article_words[0][0] + article_words[0][1::].lower()
    i = 1
    while i < len(article_words) and (article_words[i].isupper() or article_words[i].isdigit() or article_words[i] == "lt"):
        article_words[i] = article_words[i].lower()
        i += 1  
    return article_words

def find_similar(query_vector, vector_matrix):
    cosine_similarities = linear_kernel(query_vector, vector_matrix).flatten()
    related_readmes_indices = [i for i in cosine_similarities.argsort()[::-1]]
    return [(index, cosine_similarities[index]) for index in related_readmes_indices]

def sparse_vector_to_dict(sparse_vector):
    vector_dict = {}
    vector_dict['vector'] = [(int(item[0][1]),float(item[1])) for item in sparse_vector.todok().items()]
    return vector_dict

def sparse_matrix_to_dict(sparse_matrix):
    vectors_list = []
    i = 0
    for vector in sparse_matrix:
        vectors_list.append({})
        vectors_list[i]['id'] = str(i)
        vectors_list[i]['vector'] = sparse_vector_to_dict(vector)['vector']
        i += 1
    return {'vectors': vectors_list}

def get_most_similar_vectors(request):
    
    request_json = json.loads(request.replace('\'', '\"'))
    # for el in request_json['vectors']:
        # el_dict = {}
        # for k,v in el.items():
            # val = json.loads(v) if k == "vectors" else v
            # val["vector"] = eval(val["vector"])
            # el_dict[k] = val
        # vectors.append({k: el_dict})
    vectors = [{k: eval(v) if k=="vector" else v for k, v in el.items()} for el in request_json['vectors']]
    query_vector = eval(request_json['query_vector'])
    vectors_dict = {}
    coordinates = [x[0] for x in query_vector]
    values = [x[1] for x in query_vector]
    indexes = [0] * len(query_vector)
    i = 1
    for vector in vectors:
        coordinates += [x[0] for x in vector['vector']]
        values += [x[1] for x in vector['vector']]
        indexes += [i] * len(vector['vector'])
        vectors_dict[i] = vector['id']
        i += 1

    sparse_vectors_matrix = sparse.csr_matrix((values,(indexes,coordinates)))

    query_vector = sparse_vectors_matrix[0]

    index_similarity_pairs = find_similar(query_vector, sparse_vectors_matrix[1:])

    id_similarity_pairs = [(vectors_dict[x[0] + 1], x[1]) for x in index_similarity_pairs]

    return id_similarity_pairs
