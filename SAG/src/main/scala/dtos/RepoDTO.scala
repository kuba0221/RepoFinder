package dtos

case class  RepoDTO
(
  name: String,
  similarity: Double,
  url: String,
  description: String
)

