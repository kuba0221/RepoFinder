package dtos

case class RepoWithVector
(
  name: String,
  vector: String,
  url: String,
  description: String
)
