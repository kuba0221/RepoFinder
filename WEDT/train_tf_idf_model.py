import argparse
import pickle
from nlp_tools import get_training_documents, tokenize, preprocess
from sklearn.feature_extraction.text import TfidfVectorizer

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--model", type=str, default='tf_idf_model',
                        help="name of tf-idf model")
    parser.add_argument("-j", "--json_directory", type=str, default='',
                        help="path to directory with readme files in json format")
    parser.add_argument("-l", "--lower", type=int, default=1,
                        help="lower range of ngrams")
    parser.add_argument("-u", "--upper", type=int, default=3,
                        help="upper range of ngrams")
    args = parser.parse_args()
    return args

if __name__ == '__main__':

    args = parse_arguments()
    
    print ("Getting training documents")

    training_documents = get_training_documents(args.json_directory)

    tf_idf_vectorizer = TfidfVectorizer(analyzer = 'word', preprocessor = preprocess, tokenizer = tokenize, ngram_range=(1,3), min_df = 0, stop_words = 'english')

    print ("Training model")

    tf_idf_vectorizer.fit(training_documents)

    with open(args.model, 'wb') as f:
        pickle.dump(tf_idf_vectorizer, f)
