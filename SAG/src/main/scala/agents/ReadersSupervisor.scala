package agents

import java.io.File

import akka.actor.{Actor, ActorLogging, Props}
import dtos.RepoDTO
import enums.SysMessages._

import scala.collection.mutable.ListBuffer

object ReadersSupervisor {
  def props(actorPool: Int, readerCapacity: Int, topRepos: Int) = Props(new ReadersSupervisor(actorPool, readerCapacity, topRepos))
}

class ReadersSupervisor(actorPool: Int, readerCapacity: Int, topRepos: Int) extends Actor with ActorLogging {

  var files: Array[File] = _
  var pendingReaderActors = 0
  var vector: String = _
  var dir: String = _
  var topN: Int = _
  var mostSimilar: List[RepoDTO] = _

  override def preStart: Unit = {
    for(i <- 1 to actorPool)
      context.actorOf(RepoReader.props(), name = s"reader$i")
  }

  def receive: PartialFunction[Any, Unit] = {
    case Directory(base, vec) =>
      vector = vec
      files = base.listFiles
      topN = topRepos
      pendingReaderActors = 0
      for(i <- 1 to actorPool if files.length > 0) {
        pendingReaderActors += 1
        context.child(s"reader$i").get ! GetSimilar(vec, files.take(readerCapacity))
        files = files drop readerCapacity
      }

    case MostSimilar(repos) =>
      pendingReaderActors -= 1
      if(mostSimilar == null)
        mostSimilar = repos take topN
      else {
        mostSimilar = ((mostSimilar ::: repos)sortWith (_.similarity > _.similarity)) take topN
      }
      if(files.length > 0) {
        sender() ! GetSimilar(vector, files.take(readerCapacity))
        files = files.drop(readerCapacity)
        pendingReaderActors += 1
      }
      else if(pendingReaderActors == 0) {
        for((repo, i) <- mostSimilar.zipWithIndex) {
          println("[%d] name: %s, URL: %s, similarity: %.3f".format(i, repo.name, repo.url, repo.similarity))
        }
        context.parent ! Finished
//        context.system.terminate()
      }
  }
}

