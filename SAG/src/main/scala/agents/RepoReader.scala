package agents

import HttpLayer.HttpLayer
import enums.SysMessages._
import akka.actor.{Actor, ActorLogging, Props}
import dtos.{RepoDTO, RepoJson, RepoWithVector}
import org.json4s._
import org.json4s.jackson.JsonMethods._

import scala.collection.mutable.ListBuffer
object RepoReader {
  def props() = Props(new RepoReader())
}

class RepoReader() extends Actor with ActorLogging {

  import context.dispatcher

  override def preStart: Unit = {
    log.info("RepoReader Actor initialized")
  }

  def receive: PartialFunction[Any, Unit] = {
    case GetSimilar(vector, files) =>
      var repos = new ListBuffer[RepoWithVector]()
      for(file <- files) {
        val source = scala.io.Source.fromFile(file)
        val json = parse(source.reader())
        implicit val formats: DefaultFormats.type = DefaultFormats
        val extracted = json.extract[RepoJson]
        repos += RepoWithVector(extracted.name, extracted.vector, extracted.url, extracted.description)
      }
      val similar = HttpLayer.getSimilar(vector, repos.toList)
      var mostSimilar = new ListBuffer[RepoDTO]()
      for(el <- similar) {
        val name = repos.find(_.name.toString == el.id).get.name
        val url = repos.find(_.name.toString == el.id).get.url
        val description = repos.find(_.name.toString == el.id).get.description
        mostSimilar += RepoDTO(name, el.similarity, url, description)
      }
      log.info("Found most similar!")
      context.parent ! MostSimilar(mostSimilar.toList)
  }
}
