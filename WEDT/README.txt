SAG/WEDT project: System for searching open source projects based on information from code repositories

Requirements:
* pip installed
* some repositories in json format to train the model

Installation:

sudo pip install virtualenv

bash setup.sh

Model training:

source venv/bin/activate

python train_tf_idf_model.py -m ${NAME_OF_MODEL} -j ${DIRECTORY_WITH_JSONS} -l ${LOWER_NGRAM_RANGE} -u ${UPPER_NGRAM_RANGE}

deactivate

Script loads reuters training articles and repositories in jsons from ${DIRECTORY_WITH_JSONS} to train tf-idf model using skleran TfidfVectorizer. Text is tokenized using simple Regex Tokenizer and stemmed using Porter Stemmer from nltk library. Default range of ngrams: ${LOWER_NGRAM_RANGE} = 1 and ${UPPER_NGRAM_RANGE} = 3. If ${PATH_TO_DIRECTORY_WITH_JSONS} is empty only reuters corpus will be used for training. Returns pickled model named ${NAME_OF_MODEL}.

Model testing:

source venv/bin/activate

python test_tf_idf_model.py -m ${NAME_OF_MODEL} -j ${DIRECTORY_WITH_JSONS} -i ${INDEX_OF_INPUT} -n ${NUMBER_TO_COMPARE}

deactivate

Loads pretrained model ${NAME_OF_MODEL} and vectorizes repositories from ${DIRECTORY_WITH_JSONS} (or reuters test articles if empty). Then calculates cosine similarity between test document chosen by integer ${INDEX_OF_INPUT} (random if empty or out of range). Print chosen repository/artcile and ${NUMBER_TO_COMPARE} most similar ones.

Server:

Running:

source venv/bin/activate

python server.py ${NAME_OF_MODEL}

Runs a simple Flask api to communicate with Akka part of project.

Calculating vectors:

curl http://localhost:5000/vector -d "text=Tf-idf is the best in searches of information retrieval" -X GET

Text of query to be vectorized should be specified in text parameter. Returns a json with a list of tuples, first elements being coordinates and second - their values.

Comparing vectors:

curl http://localhost:5000/similar -d "json=`cat sample_request.json`"

Calculates cosine similarity of vectors from json given as an input. One of the vectors with id specified in "query_id" is the one other are compared to. Returns a sorted list of id, cosine similarity pairs of "number" most similar vectors. If "number" is not specified returns entire list. Sample json should be in the same directory as this Readme.

P.S. This Readme is written in English solely for the purpose of vectorizing it. In fact, its vector is included in sample request json.
