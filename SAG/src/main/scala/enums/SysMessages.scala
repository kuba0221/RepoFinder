package enums

import java.io.File

import dtos.{RepoDTO, RepoJson, RepoWithVector}

object SysMessages {
  case class Directory(dir: File, query: String)
  case class ReadFiles(files: Array[File])
  case class Repo(value: RepoJson)
  case class CalcRepo(value: List[RepoWithVector])
  case class GetSimilar(vector: String, files: Array[File])
  case class MostSimilar(repos: List[RepoDTO])
  case class Finished()
}

