import json
import argparse
from random import randint
from nlp_tools import get_repositories_json_files, get_reuters_articles, Readme_tf_idf, Repository, find_similar, is_ascii

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--model", type=str, required=True,
                        help="path to tf-idf model")
    parser.add_argument("-j", "--json_directory", type=str, default='',
                        help="path to directory with readme files in json format")
    parser.add_argument("-i", "--input", type=int, default=0,
                        help="index of readme chosen to be an input")
    parser.add_argument("-n", "--number_to_compare", type=int, default=2,
                        help="number of most similar readmes to output")
    args = parser.parse_args()
    return args

if __name__ == '__main__':

    args = parse_arguments()

    print ("Loading tf-idf model")

    Readme_tf_idf_model = Readme_tf_idf(args.model)

    print ("Getting test documents")

    if args.json_directory:
        json_list = get_repositories_json_files(args.json_directory)
        test_documents = [json['readme'] for json in json_list[:1000] if is_ascii(json['readme'])]
    else:
        test_documents = get_reuters_articles("test")

    try:
        index_not_out_of_range = test_documents[args.input]
        input_index = args.input
    except IndexError:
        input_index = randint(0, len(test_documents)-1)

    print ("Vectorizing test documents")

    test_documents_vectorized = Readme_tf_idf_model.vectorize(test_documents)

    similar_documents = find_similar(test_documents_vectorized[input_index], test_documents_vectorized)[1:args.number_to_compare + 1]

    if args.json_directory:
        print ("Chosen repository:\n")
        chosen_json = json_list[input_index]
        print (Repository(chosen_json['name'], chosen_json['readme'], chosen_json['description'])) 

        print("\nMost similar repositories:\n")
        for similar_document in similar_documents:
            repository_json = json_list[similar_document[0]]
            print ("\ncosine similarity: " + str(similar_document[1]) + "\n")
            print (Repository(repository_json['name'], repository_json['readme'], repository_json['description']))
    else:
        print ("Chosen article:\n")
        print (test_documents[input_index]) 

        print("\nMost similar articles:\n")
        for similar_document in similar_documents:
            print ("\ncosine similarity: " + str(similar_document[1]) + "\n")
            print (test_documents[similar_document[0]])
