package dtos

import java.sql.Timestamp

case class  RepoJson
(
  dateDownloaded: Timestamp,
  readme: String,
  name: String,
  vector: String,
  url: String,
  description: String
)