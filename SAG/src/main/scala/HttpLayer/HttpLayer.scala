package HttpLayer
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.Uri.Query
import akka.http.scaladsl.model._
import akka.stream.ActorMaterializer
import akka.actor.ActorSystem
import akka.http.scaladsl.unmarshalling.Unmarshal
import app.Application

import scala.concurrent.{Await, ExecutionContextExecutor}
import scala.concurrent.duration._
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import spray.json.DefaultJsonProtocol._
import spray.json._
import HttpMethods._
import dtos.{RepoWithVector, SimilarityDTO}
import org.json4s._

import scala.collection.mutable.ListBuffer

object HttpLayer {

  implicit val system: ActorSystem = Application.sys
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher

  final case class Vec(vector: List[(Int, Double)])
  final case class SimilarVectors(id: String, vector: String)

  final case class SimilarVectorsWrapper(query_vector: String, vectors: Array[JsValue])

  final case class JsonWrapper(json: JsValue)

  implicit val vectorFormat: RootJsonFormat[Vec] = jsonFormat1(Vec)
  implicit val similarVectorsFormat: RootJsonFormat[SimilarVectors] = jsonFormat2(SimilarVectors)
  implicit val similarVectorsWrapperFormat: RootJsonFormat[SimilarVectorsWrapper] = jsonFormat2(SimilarVectorsWrapper)
  implicit val jsonWrapperFormat: RootJsonFormat[JsonWrapper] = jsonFormat1(JsonWrapper)
  implicit val similarityDtoFormat: RootJsonFormat[SimilarityDTO] = jsonFormat2(SimilarityDTO)

  def getVector(text: String): String = {
    val parameters = Map("text" -> text)
    var vector = ""
    Await.result({
      Http().singleRequest(HttpRequest(uri = Uri("http://localhost:5000/vector").withQuery(Query(parameters)))).map {
        value =>
          Await.result({
            Unmarshal(value).to[Vec].map { json =>
              vector = "["+json.vector.mkString(",")+"]"
            }
          }, Duration.Inf)
      }
    }, Duration.Inf)
    vector
  }

  def getSimilar(query: String, repos: List[RepoWithVector]): List[SimilarityDTO] = {
    var similarity = new ListBuffer[SimilarityDTO]
    var vectors = new ListBuffer[JsValue]
    implicit val formats: DefaultFormats.type = DefaultFormats
    repos.foreach { elem =>
      vectors += SimilarVectors(elem.name.toString, elem.vector).toJson
    }
    val parameters = SimilarVectorsWrapper(query, vectors.toArray)
    val json = JsonWrapper(parameters.toJson)
    val entityJson = HttpEntity(ContentTypes.`application/json`, json.toJson.toString)
    Await.result({
      Http().singleRequest(HttpRequest(POST, uri = Uri("http://localhost:5000/similar"), entity = entityJson)).map {
        value =>
          Await.result({
            Unmarshal(value).to[List[(String, Double)]].map { results =>
              results.foreach({ elem =>
                similarity += SimilarityDTO(elem._1, elem._2)
              })
            }
          }, Duration.Inf)
      }
    }, Duration.Inf)
    similarity.toList
  }

}
