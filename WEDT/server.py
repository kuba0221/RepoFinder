import sys
import json
from flask import Flask, jsonify
from flask_restful import Api, Resource, reqparse, abort
from nlp_tools import Readme_tf_idf, sparse_vector_to_dict, get_most_similar_vectors

app = Flask(__name__)
api = Api(app)

class Vector(Resource):
    def get(self):
        args = parser.parse_args()

        if args['text'] is None:
            abort(404, message="Text field is empty")

        resp = sparse_vector_to_dict(Readme_tf_idf_model.vectorize([args['text']]))

        resp = app.response_class(response=json.dumps(resp), status=200, mimetype='application/json')
        resp.headers['Connection'] = 'close'
        return resp

class Similar(Resource):

    def post(self):

        args = parser.parse_args()

        if args['json'] is None:
            abort(404, message="Json field is empty")

        resp = get_most_similar_vectors(args['json'])
        print(resp)
        resp = app.response_class(response=json.dumps(resp), status=200, mimetype='application/json')
        resp.headers['Connection'] = 'close'
        return resp

parser = reqparse.RequestParser()
parser.add_argument('text')
parser.add_argument('json')

api.add_resource(Vector, '/vector')
api.add_resource(Similar, '/similar')

if __name__ == '__main__':

    Readme_tf_idf_model = Readme_tf_idf(sys.argv[1])
    app.run(debug=True)

