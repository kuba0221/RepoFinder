package app
import java.io.File

import agents.ReadersSupervisor
import akka.actor._

import scala.concurrent.Await
import scala.io.StdIn

object Application extends App {
  import enums.SysMessages._

  val sys = ActorSystem()
  import akka.util.Timeout

  import scala.concurrent.duration._
  implicit val timeout: Timeout = Timeout(5 minutes)

  val supervisor = sys.actorOf(ReadersSupervisor.props(5, 100, 10), name = "forker")
  val path = "path_to_repos"
  while(true) {
    println("Search for repository:")
    var vector: String = ""
    var query: String = ""
    do {
      query = StdIn.readLine()
      vector = HttpLayer.HttpLayer.getVector(query)
    } while (vector.length < 3)
    import akka.pattern.ask
    val future = supervisor ? Directory(new File(path), vector)
    val result = Await.result(future, timeout.duration)

    //needs to wait for result and continue
  }
}